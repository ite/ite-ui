Alert is an AngularJS-version of bootstrap's alert.

This directive can be used to generate alerts from the dynamic model data (using the ng-repeat directive);

The presence of the "close" attribute determines if a close button is displayed

**ui.ite**

We added a timer to the directive that will remove the alert after a set amount of seconds.
