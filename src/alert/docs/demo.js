function AlertDemoCtrl($scope, AlertService) {
  $scope.alerts = [
    { type: 'danger', heading: 'Error!', message: 'Oh snap! Change a few things up and try submitting again.' },
    { type: 'info', heading: 'Info!', message: 'New modules coming soon.' },
    { type: 'success', heading: 'Success!', message: 'Well done! You successfully read this important alert message.' }
  ];

  $scope.addAlert = function() {
    //$scope.alerts.push({class: 'warning', heading: 'Warning:', message: "Example of a warning alert."});
    AlertService.prepAlert({
        type: 'warning',
        heading: 'Warning:',
        message: 'Example of a warning alert.'
    });
  };

}