angular.module("ui.ite.alert", [])

.controller('AlertController', function ($scope, $timeout, AlertService) {

    var timeout;

    $scope.alert = {
        type: '',
        heading: '',
        message: ''
    };
    $scope.$on('handleAlert', function() {  // If anything happen on alert broadcast, execute follow code...
        $scope.alert = AlertService.alert;

        if ($scope.alert !== { type: '', heading: '', message: '' }) {

            if (timeout) {
                $timeout.cancel(timeout);
            }

            timeout = $timeout(function() {
                AlertService.clearAlert();
            }, 2500);
        }
    });
})

.directive('alertBox', function () {
    return {
        restrict: 'EA',
        controller: 'AlertController',
        templateUrl: 'template/alert/alert.html',
        transclude: true,
        replace: true,
        scope: {
            type: '='
        }
    };
})

.factory('AlertService', function($rootScope) {
    var alertService = {};

    alertService.alert = {
        type: '',
        heading: '',
        message: ''
    };

    alert.prepAlert = function(alertConfig) {
        this.alert = alertConfig;
        this.broadcastAlert();
    };

    alert.clearAlert = function() {
        this.alert = { type: '', heading: '', message: '' };
        this.broadcastAlert();
    };

    alert.broadcastAlert = function() {
        $rootScope.$broadcast('handleAlert');
    };

    return alert;
});
