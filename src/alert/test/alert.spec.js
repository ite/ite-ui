describe("alert", function () {

  var scope, $compile;
  var element;

  beforeEach(module('ui.ite.alert'));
  beforeEach(module('template/alert/alert.html'));

  beforeEach(inject(function ($rootScope, _$compile_, $controller) {

    scope = $rootScope;
    $compile = _$compile_;

    element = angular.element(
        "<div>" + 
          "<alert-box ng-repeat='alert in alerts' type='alert.type'>{{alert.msg}}</alert-box>" +
        "</div>");

    scope.alerts = [
      { prefix: 'Success!', msg:'foo', type:'success'},
      { prefix: 'Error!', msg:'bar', type:'error'},
      { prefix: 'Warning!', msg:'baz'}
    ];
  }));

  function createAlerts() {
    $compile(element)(scope);
    scope.$digest();
    return element.find('.alert');
  }

  function findContent(index) {
    return element.find('span').eq(index);
  }

  it("should generate alerts using ng-repeat", function () {
    var alerts = createAlerts();
    expect(alerts.length).toEqual(3);
  });

  it("should use correct classes for different alert types", function () {
    var alerts = createAlerts();
    expect(alerts.eq(0)).toHaveClass('alert-success');
    expect(alerts.eq(1)).toHaveClass('alert-error');
    expect(alerts.eq(2)).toHaveClass('alert-warning');
  });

  it('should show the alert content', function() {
    var alerts = createAlerts();

    for (var i = 0, n = alerts.length; i < n; i++) {
      expect(findContent(i).text()).toBe(scope.alerts[i].msg);
    }
  });

  it('should be possible to add additional classes for alert', function () {
    var element = $compile('<alert-box class="alert-block" type="\'info\'">Default alert!</alert-box>')(scope);
    scope.$digest();
    expect(element).toHaveClass('alert-block');
    expect(element).toHaveClass('alert-info');
  });

});
