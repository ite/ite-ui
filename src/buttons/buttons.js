angular.module('ui.ite.buttons', ['ui.ite.glyphicon'])

.constant('buttonConfig', {
  activeClass: 'active',
  toggleEvent: 'click'
})
.controller('ButtonsController', ['buttonConfig', function(buttonConfig) {
  this.activeClass = buttonConfig.activeClass || 'active';
  this.toggleEvent = buttonConfig.toggleEvent || 'click';
}])

.directive('btnRadio', function () {
  return {
    require: ['btnRadio', 'ngModel'],
    controller: 'ButtonsController',
    link: function (scope, element, attrs, ctrls) {
      var buttonsCtrl = ctrls[0], ngModelCtrl = ctrls[1];

      //model -> UI
      ngModelCtrl.$render = function () {
        element.toggleClass(buttonsCtrl.activeClass, angular.equals(ngModelCtrl.$modelValue, scope.$eval(attrs.btnRadio)));
      };

      //ui->model
      element.bind(buttonsCtrl.toggleEvent, function () {
        if (!element.hasClass(buttonsCtrl.activeClass)) {
          scope.$apply(function () {
            ngModelCtrl.$setViewValue(scope.$eval(attrs.btnRadio));
            ngModelCtrl.$render();
          });
        }
      });
    }
  };
})

.directive('btnCheckbox', function () {
  return {
    require: ['btnCheckbox', 'ngModel'],
    controller: 'ButtonsController',
    link: function (scope, element, attrs, ctrls) {
      var buttonsCtrl = ctrls[0], ngModelCtrl = ctrls[1];

      function getTrueValue() {
        return getCheckboxValue(attrs.btnCheckboxTrue, true);
      }

      function getFalseValue() {
        return getCheckboxValue(attrs.btnCheckboxFalse, false);
      }
      
      function getCheckboxValue(attributeValue, defaultValue) {
        var val = scope.$eval(attributeValue);
        return angular.isDefined(val) ? val : defaultValue;
      }

      //model -> UI
      ngModelCtrl.$render = function () {
        element.toggleClass(buttonsCtrl.activeClass, angular.equals(ngModelCtrl.$modelValue, getTrueValue()));
      };

      //ui->model
      element.bind(buttonsCtrl.toggleEvent, function () {
        scope.$apply(function () {
          ngModelCtrl.$setViewValue(element.hasClass(buttonsCtrl.activeClass) ? getFalseValue() : getTrueValue());
          ngModelCtrl.$render();
        });
      });
    }
  };
})

.directive('myDirective', function() {
	return {
		restrict: 'EAC',
		replace: true,
		scope: {
			myUrl: '@', // binding strategy
			myLinkText: '@' // binding strategy
		},
		template: '<div><a class="btn btn-danger" href="{{myUrl}}">{{myLinkText}}</a></div>'
	};
})

.directive('lockBtn', function() {
	return {
		restrict: 'EAC',
		replace: true,
		scope: {
			myBtn: '=myBtn',
			myIndex: '@myIndex',
			btnLocked: '@locked',
			btnClass: '@',
			btnTitle: '@',
			btnGlyph: '@'
		},
		controller: function ( $scope ) {
		},
		link: function ( $scope ) {
			$scope.toggleLocked = function ( btn, descr ) {
				btn.locked = (btn.locked == 1) ? 0 : 1;
			};
		},
		template: '<button type="button" ng-click="toggleLocked(myBtn)" class="btn {{btnClass}} btn-lock{{myIndex}}" title="{{btnTitle}}" locked="{{btnLocked}}"><gi class="glyph" i="{{btnGlyph}}"></gi></button>'
	};
})

.directive('deleteBtn', function() {
	return {
		restrict: 'EAC',
		replace: true,
		scope: {
			myBtn: '=myBtn',
			myIndex: '@myIndex',
			btnDeleted: '@deleted',
			btnClass: '@',
			btnTitle: '@'
		},
		controller: function ( $scope ) {
		},
		link: function ( $scope ) {
			$scope.toggleDeleted = function ( btn, descr ) {
				btn.deleted = (btn.deleted == 1) ? 0 : 1;
			};
		},
		template: '<button type="button" ng-click="toggleDeleted(myBtn)" class="btn {{btnClass}} btn-delete{{myIndex}}" title="{{btnTitle}}" deleted="{{btnDeleted}}"><gi class="glyph" i="trash"></gi></button>'
	};
})

.directive('hideBtn', function() {
	return {
		restrict: 'EAC',
		replace: true,
		scope: {
			myBtn: '=myBtn',
			myIndex: '@myIndex',
			btnHidden: '@hidden',
			btnClass: '@',
			btnTitle: '@',
			btnGlyph: '@'
		},
		controller: function ( $scope ) {
		},
		link: function ( $scope ) {
			$scope.toggleHidden = function ( btn, descr ) {
				btn.hidden = (btn.hidden == 1) ? 0 : 1;
			};
		},
		template: '<button type="button" ng-click="toggleHidden(myBtn)" class="btn {{btnClass}} btn-hide{{myIndex}}" title="{{btnTitle}}" hidden="{{btnHidden}}"><gi class="glyph" i="{{btnGlyph}}"></gi></button>'
	};
})
;
