angular.module('ui.ite.glyphicon', [])
.directive("gi", function() {
	return {
		restrict : "E",
		replace : true,
		transclude : true,
		scope : {
			i : "@"
		},
		template : "<span class=\"glyphicon glyphicon-{{i}} icon-{{i}}\"></span>"
	};
});
