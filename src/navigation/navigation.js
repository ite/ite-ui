angular.module('ui.ite.navigation', ['ui.ite.dropdownToggle'])

.controller('NavigationController', ['$scope', '$attrs', function ($scope, $attrs) {
		$scope.openModal = function ( modal ) {
			var modalInstance = $modal.open({
				scope: $scope,
				backdrop: 'static',
				templateUrl: modal + '.html'
			});
		};
}])

		.directive("navigation", ['$compile',function ($compile) {

			return {
				restrict: 'E',
				replace: true,
				scope: {
					menu: '='
				},
				controller: 'NavigationController',
				template: '<ul>' +
								'<li ng-repeat="item in menu" class="{{item.children.length > 0 ? \'dropdown\' : \'\'}} {{item.type == \'divider\' ? \'divider\' : \'\'}}">' +
									'<a ng-if="item.children.length == 0 && (item.type != \'divider\' && item.type != \'modal\')" href="{{item.href}}" class="{{item.class}}">{{item.name}}</a>' +
									'<a ng-if="item.children.length == 0 && (item.type != \'divider\' && item.type != \'item\')" ng-click="openModal(item.modal)" class="{{item.class}}">{{item.name}}</a>' +
									'<a ng-if="item.children.length > 0 && (item.type != \'divider\' && item.type != \'modal\')" class="dropdown-toggle" data-toggle="dropdown">{{item.name}} <span class="caret"></span></a>' +
									'<navigation ng-if="item.children.length > 0" menu="item.children" class="dropdown-menu"></navigation>' +
								'</li>' +
							'</ul>',
				compile: function (el) {
					var contents = el.contents().remove();
					var compiled;
					return function(scope,el){
						if(!compiled) {
							compiled = $compile(contents);
						}

						compiled(scope,function(clone){
							el.append(clone);
						});
					};
				}
			};

		}]);
