function NavigationDemoCtrl($scope) {
	$scope.menu = [{
		type: "item",
		name: "Contact",
		children: [{
			type: "item",
			name: "Testing",
			children: []
		},{
			type: "divider",
			name: "",
			children: []
		},{
			type: "item",
			name: "More Testing",
			children: []
		}]
	},{
		type: "item",
		name: '2nd Menu',
		children: [{
			type: "item",
			name: "Testing",
			children: []
		},{
			type: "modal",
			name: "Open Modal",
			modal: "help-modal",
			children: []
		}]
	}];
}